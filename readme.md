# WorldTicket Test
We would like to see your coding style. *Please do not copy solution from the internet.*

## Pre-requisite
- [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- [Maven](https://maven.apache.org/download.cgi)

## Requirement
Create a simple web application for currency conversion.
The application should have the following features:

- Database to store the currency exchange rates
- REST API to retrieve the exchange rate and calculate the result
- Business Logic to offer cheaper rate for large volume selling or more expensive rate for small volume buying
